require 'rails_helper'

RSpec.describe Article, type: :model do
  let(:article) { described_class.create(title: title, text: text) }
  let(:title) { 'Awesome article' }
  let(:text) { 'Awesome tips!' }

  it 'is persisted' do
    expect(article).to be_persisted
  end

  context 'when title is too short' do
    let(:title) { 'abc' }

    it 'is not persisted' do
      expect(article).not_to be_persisted
    end
  end

  describe '#empty_text?' do
    subject { article.empty_text? }

    it 'returns false' do
      is_expected.to be_falsy
    end

    context 'when text is empty' do
      let(:text) { '' }

      it 'returns true' do
        is_expected.to be_truthy
      end
    end
  end
end
